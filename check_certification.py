"""
SYNOPSIS
	check_certification.py -v

DESCRIPTION
   Blah Blah

EXAMPLES

	check_certification -v


EXIT STATUS


AUTHOR
	Mark Rippon

LICENSE
	Copyright Forestry Tasmania 2017

VERSION
	x.0
"""
import cx_Oracle
import logging
import os
import time
from optparse import OptionParser
import configparser


def load_config(ini_file):
	""" returns a dictionary with keys of the form <section>.<option> and the corresponding values"""
	config = {}
	try:
		cp = configparser.ConfigParser()
		cp.read(ini_file)
		for sec in cp.sections():
			name = sec.lower()
			for opt in cp.options(sec):
				config[name + "." + opt.lower()] = str.strip(cp.get(sec, opt))

	except FileNotFoundError:
		logging.fatal('Ini file does not exist!')

	return config


def create_database_connection(dbconnect_string):
	""" creates a database connection and returns it"""
	try:
		con = cx_Oracle.connect(dbconnect_string)
	except cx_Oracle.DatabaseError as e:
			error, = e.args
			if error.code == 1017:
				logging.error('Please check your credentials.')
			else:
				logging.error('Database connection error: %s'.format(e))
			# Very important part!
			raise

	logging.debug("Connected successfully")

	return con


def do_get_sql(sql, dbconn):
	"""do a get sql ie update - return reocrds"""
	res = 0
	try:
		get_cur = cx_Oracle.Cursor(dbconn)
		resultset = get_cur.execute(sql)
		for row in resultset:
			res = row[0]

	except cx_Oracle.DatabaseError as e:
		error, = e.args
		logging.debug(error.code)
		logging.debug(error.message)

	return res


def do_set_sql(sql, dbconn):
	"""do an action sql ie update, but return nothing"""
	try:
		set_cur = cx_Oracle.Cursor(dbconn)
		set_cur.execute(sql)
		set_cur.close()
		dbconn.commit()
	except cx_Oracle.DatabaseError as e:
		error, = e.args
		logging.debug(error.code)
		logging.debug(error.message)

	return


def main():
	# ---- set system type variables ----
	start_time = time.time()

	# ---- Option parser ----
	parser = OptionParser(usage=globals()['__doc__'], version='$Id$', add_help_option=False)
	parser.add_option("-h", "--help", action="help")
	parser.add_option("-v", action="store_true", dest="verbose", help="Run in verbose (DEBUG) mode")
	# add script-specific options here..
	parser.add_option("-o", dest="operation_id", help="FOD Operation Id")
	parser.add_option("-t", dest="cert_type", help="Certification type")

	(options, args) = parser.parse_args()

	if options.verbose:
		log_level = logging.DEBUG
	else:
		log_level = logging.INFO

	# ---- ----

	# Get ini settings
	cfg = load_config('C:\GitRepository\AFSCertified\check_certification.ini')

	# create logger for process
	logging.basicConfig(level=log_level, format='%(asctime)s %(levelname)-5s %(message)s', datefmt='%m-%d %H:%M',
	                    filename=os.path.join(os.getcwd(), cfg['general.log']), filemode='w')

	# define a Handler which writes DEBUG messages or higher to the sys.stderr (console)
	console = logging.StreamHandler()
	console.setLevel(logging.DEBUG)
	# set a format which is simpler for console use
	formatter = logging.Formatter('%(levelname)-5s %(funcName)25s() :  %(message)s')
	# tell the handler to use this format
	console.setFormatter(formatter)
	# add the handler to the root logger
	logging.getLogger('').addHandler(console)

	# Log Header
	logging.info(' ------- Script Started -------')

	try:
		logging.debug('Main code..')
		# validate check type
		if options.cert_type not in ('AFS', 'CW'):
			logging.fatal("Certification type should be 'AFS' or 'CW'")
			raise SystemExit

		ro_dbconstr = cfg['database.ro_connect_string']
		rw_dbconstr = cfg['database.rw_connect_string']
		# get area of source shape
		ro_connection = create_database_connection(ro_dbconstr)
		opsql = cfg['fod.src_sql'].format(options.operation_id)
		operation_area = float(do_get_sql(opsql, ro_connection))
		logging.info('Area for Operation: {0} is: {1}Ha'.format(options.operation_id, operation_area))
		# now do check
		chksqlkey = '{0}.src_sql'.format(options.cert_type.lower())
		checksql = cfg[chksqlkey].format(options.operation_id)
		thresholdkey = '{0}.threshold'.format(options.cert_type.lower())
		threshold = float(cfg[thresholdkey])
		updsqlkey = '{0}.upd_sql'.format(options.cert_type.lower())
		updsql = cfg[updsqlkey].format(options.operation_id)
		test_area = float(do_get_sql(checksql, ro_connection))
		logging.info('Area that is {0} certified: {1}Ha'.format(options.cert_type, test_area))
		proportion = round(test_area/operation_area, 4)
		ro_connection.close()

		if proportion >= threshold:
			logging.debug('{0} Certified: {1}%'.format(options.cert_type, proportion*100))
			rw_connection = create_database_connection(rw_dbconstr)
			do_set_sql(updsql, rw_connection)
			rw_connection.close()
			logging.debug('Operation ID: {0} updated'.format(options.operation_id))
		else:
			logging.debug('NOT {0} Certified: {1}%'.format(options.cert_type, proportion*100))

	except KeyboardInterrupt as kbdint:  # Ctrl-C
		raise kbdint
	except SystemExit as sysexit:  # sys.exit()
		raise sysexit
	except Exception as e:
		logging.fatal('ERROR, UNEXPECTED EXCEPTION')
		logging.fatal(str(e))
		# os._exit(1)

	finally:
		# cleanup any open dbase connections etc here

		logging.info(time.asctime())
		logging.info('TOTAL TIME IN SECONDS: {0}'.format(round(time.time() - start_time),4))
		logging.info("-------- Script Completed --------")

	return

if __name__ == '__main__':
	main()
